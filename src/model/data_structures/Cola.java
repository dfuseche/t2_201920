package model.data_structures;

public class Cola<T> implements ICola<T>
{
	private Node<T> tail;
	private Node<T> head;
	int size;
	public Node<T> getTail()
	{
		return tail;
	}
	
	public Cola(){
		tail = null;
		head = null;
	}
	

	public void enqueue(T item) 
	{

		Node<T> pNew = new Node<T>(item);
		
		if(tail == null){
			tail = pNew;
			head = pNew;
		}
		else{
			head.changePrevious(pNew);
			head = pNew;
		}
		size++;
	}

	
	public T dequeue() 
	{
		
		T ans = tail.getElement();
		tail = tail.getPrevious();
		size--;
		return ans;

	}
	
	public int size()
	{
		return size;
		
	}
}
