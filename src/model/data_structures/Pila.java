package model.data_structures;

public class Pila<T> implements IStack<T>
{
	private Node<T> head;

	public Pila(){
		head = null;
	}

	@Override
	public void push(T item) {
		// TODO Auto-generated method stub
		Node<T> pNew = new Node<T>(item);
		
		if(head == null){
			head = pNew;
		}
		else{
			pNew.changeNext(head);
			head = pNew;
		}	
		
	}

	@Override
	public T pop() {
		// TODO Auto-generated method stub
		T ans = head.getElement();
		head = head.getNext();
		return ans;
	}
	
	public Node<T> getHead()
	{
		return head;
	}

}
