package model.data_structures;

public class Node<T>
{
	private T element;
	private Node<T> next;
	private Node<T> previous;
	
	public Node(T elem) 
	{
		element = elem;
		next = null;
		previous = null;
	}
	
	public T getElement()
	{
		return element;
	}
	
	public Node<T> getNext()
	{
		return next;
	}
	
	public Node<T> getPrevious()
	{
		return previous;
	}
	
	public void changeNext(Node<T> pNew)
	{
		next = pNew;
	}
	
	public void changePrevious(Node<T> pNew)
	{
		previous = pNew;
	}

}
