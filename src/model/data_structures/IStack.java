package model.data_structures;

public interface IStack<T>
{
	public void push(T parametro);
	
	
	public T pop();
}
