package model.logic;

public class Uber
{
	private double sourceId;
	
	private double dstid;
	
	private double  hod;
	
	private double meanTravel_time;
	
	private double standardDeviationTravelTime;
	
	private double geometricMeanTravel;
	
	
	private double geometricStandardDeviationTravelTime;
	
	public Uber(double sid,double dsid, double pHod, double meanT, double standart, double pgeometric, double geometricStandart)
	{
		sourceId=sid;
		dstid = dsid;
		hod = pHod;
		meanTravel_time = meanT;
		standardDeviationTravelTime = standart;
		geometricMeanTravel = pgeometric;
		geometricStandardDeviationTravelTime = geometricStandart;
	}

	public double getSourceId() {
		return sourceId;
	}

	public double getDstid() {
		return dstid;
	}

	public double getHour() {
		return hod;
	}

	public double getMeanTravel_time() {
		return meanTravel_time;
	}

	public double getStandardDeviationTravelTime() {
		return standardDeviationTravelTime;
	}

	public double getGeometricMeanTravel() {
		return geometricMeanTravel;
	}

	public double getGeometricStandardDeviationTravelTime() {
		return geometricStandardDeviationTravelTime;
	}

	
}
