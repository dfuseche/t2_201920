package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

import model.data_structures.ArregloDinamico;
import model.data_structures.Cola;
import model.data_structures.IArregloDinamico;
import model.data_structures.Node;
import model.data_structures.Pila;


/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo {
	/**
	 * Atributos del modelo del mundo
	 */
	private IArregloDinamico datos;

	private Cola  cola;
	
	private Pila pila;
	
	public MVCModelo(){
		cola = new Cola();
		pila = new Pila();
		
	}

	/**
	 * Constructor del modelo del mundo con capacidad dada
	 * @param tamano
	 */
	public MVCModelo(int capacidad)
	{
		datos = new ArregloDinamico(capacidad);
	}

	/**
	 * Servicio de consulta de numero de elementos presentes en el modelo 
	 * @return numero de elementos presentes en el modelo
	 */
	public int darTamano()
	{
		return datos.darTamano();
	}

	/**
	 * Requerimiento de agregar dato
	 * @param dato
	 */
	public void agregar(String dato)
	{	
		datos.agregar(dato);
	}

	/**
	 * Requerimiento buscar dato
	 * @param dato Dato a buscar
	 * @return dato encontrado
	 */
	public String buscar(String dato)
	{
		return datos.buscar(dato);
	}

	/**
	 * Requerimiento eliminar dato
	 * @param dato Dato a eliminar
	 * @return dato eliminado
	 */
	public String eliminar(String dato)
	{
		return datos.eliminar(dato);
	}


	public void cargar () throws Exception
	{
		String ruta = "C:\\Users\\Mchafloque\\t2_201920\\data\\bogota-cadastral-2018-1-All-HourlyAggregate.csv";
		BufferedReader br = new BufferedReader (new FileReader (ruta));
		String [] datos = new String[7];
		String Linea = br.readLine();
		Linea = br.readLine();

		while (Linea != null){
						
			datos = Linea.split(",");
			double sId = Double.parseDouble(datos[0].trim());
			double dstId = Double.parseDouble(datos[1].trim());
			double hour = Double.parseDouble(datos[2].trim());
			double mtT = Double.parseDouble(datos[3].trim());
			double sdtT = Double.parseDouble(datos[4].trim());
			double gmtT = Double.parseDouble(datos[5].trim());
			double gsdtT = Double.parseDouble(datos[6].trim());

			Uber nuevo = new Uber (sId, dstId, hour, mtT, sdtT, gmtT, gsdtT);
			cola.enqueue(nuevo);
			pila.push(nuevo);
			
			Linea = br.readLine();
		}
		
		br.close();
		
	}
	
	public Cola buscarClusterMasGrande ()
	{
		Cola resultado = new Cola();
		Cola mejor = new Cola();
		double cont = 0;
		double mayor = 0;
		boolean primero = true;
		double hodActual = -1;
		
		while( cola.getTail() != null ){
			Uber viajeActual = (Uber) cola.getTail().getElement();
			if( viajeActual.getHour() > hodActual ){
				hodActual = viajeActual.getHour();
				cont++;
				resultado.enqueue(viajeActual);
				cola.dequeue();
			}
			else{
				if( cont > mayor ){
					mayor = cont;
					mejor = new Cola();
					while(resultado.getTail() != null){
						Uber actual = (Uber) resultado.getTail().getElement();
						mejor.enqueue(actual);
						resultado.dequeue();
					}
				}
				cont = 0;
				resultado = new Cola();
				resultado.enqueue(viajeActual);
				hodActual = viajeActual.getHour();
				cont++;
				cola.dequeue();
			}
		}
			
		return mejor;
		
	}
	
	public Cola darNViajes(double N, double hour)
	{
		Cola viajes = new Cola();
		int cont = 0;
		
		while(pila.getHead() != null && cont < N ){
			cont++;
			Uber viaje = (Uber) pila.getHead().getElement();
			if(viaje.getHour() == hour ){
				viajes.enqueue(viaje);
			}
			
			pila.pop();
		}
		
		return viajes ;
	}


}
