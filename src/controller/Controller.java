package controller;

import java.io.IOException;
import java.util.Scanner;

import model.data_structures.Cola;
import model.data_structures.Pila;
import model.logic.MVCModelo;
import model.logic.Uber;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;
	
	/* Instancia de la Vista*/
	private MVCView view;
	
	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}
		
	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		String dato = "";
		String respuesta = "";

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
				case 1:
					System.out.println("--------- \nCargar: ");
					try {
						modelo.cargar();
					} catch (Exception e) {
						e.printStackTrace();
					}					
					break;

				case 2:
					System.out.println("--------- \n Dar cluster m�s grande: ");
					Cola nueva = modelo.buscarClusterMasGrande();
					int cont = 0;
					
					while( nueva.getTail() != null ){
						cont++;
						Uber actual = (Uber) nueva.getTail().getElement();
						System.out.println("Hora del viaje: " + actual.getHour());
						System.out.println("Zona de Origen: " + actual.getSourceId());
						System.out.println("Zona de Destino: " + actual.getDstid());
						System.out.println("Tiempo promedio: " + actual.getMeanTravel_time());
						nueva.dequeue();
					}
					
					System.out.println("El cluster m�s grande es de: " + cont + " viajes.");
					break;

				case 3:
					System.out.println("Digite la cantidad de viajes a consultar (N)");
					double N = lector.nextDouble();
					System.out.println("Digite la hora a consultar. ");
					double hour = lector.nextDouble();
					Cola viajes = modelo.darNViajes(N, hour);
					
					while( viajes.getTail() != null ){
						Uber actual = (Uber) viajes.getTail().getElement();
						System.out.println("Hora del viaje: " + actual.getHour());
						System.out.println("Zona de Origen: " + actual.getSourceId());
						System.out.println("Zona de Destino: " + actual.getDstid());
						System.out.println("Tiempo promedio: " + actual.getMeanTravel_time());
						viajes.dequeue();
					}
					
					break;	

				default: 
					System.out.println("--------- \n Opcion Invalida !! \n---------");
					break;
			}
		}
	} 
	
}
